$(document).ready(function () {
    $('.menu-link').click(function () {
        $('.scroll-rem').removeClass('scroll-pad');
        $($(this).data('target')).addClass('scroll-pad');
        $('body,html').animate({
            scrollTop: $($(this).data('target')).offset().top
        }, 1000);
    });
});

$(document).ready(function () {
    $('.hide').click(function () {
$('#mostPopClie').slideToggle(1000);
    })
});

$(document).ready(function () {
    $(window).scroll(function () {
        const winHeight = $(window).height();
        if($(this).scrollTop() > winHeight){
            $('.slide-Top').fadeIn()
        }
        else {
            $('.slide-Top').fadeOut()
        }
    });
    $('.slide-Top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1000)
    })
});